#!/usr/bin/env bash

##################################
#        Global Variables        #
##################################
# Constants
readonly thisScript=$(basename $0 | sed 's/\.sh//g')
readonly default="\033[39m"
readonly black="\033[30m"
readonly red="\033[31m"
readonly green="\033[32m"
readonly yellow="\033[33m"
readonly blue="\033[34m"
readonly argsLength=$#

# Variables
pcap="$1"
hostIP="$2"
proxy="54.208.204.191"
digesterProxy="54.85.142.150"
saltMaster="54.209.181.132"
grandTotal="0"
advIP=""
debugMode=""

if [[ "$argsLength" == "3" ]]; then
  if [[ "$3" == "-d" || "$3" == "debug" ]]; then
    debugMode="$3"
  else
    advIP="$3"
  fi

elif [[ "$argsLength" == "4" ]]; then
  debugMode="$4"
  advIP="$3"
fi


##################################
#           Functions            #
##################################
function log () {
    case "$1" in
        err)
            echo -e    "$red[$( date )] [$(basename $0)] [ERROR  ]$default $2" | tee -a /tmp/biobrain-init.log
            ;;
        info)
              echo -e "$blue[$( date )] [$(basename $0)] [INFO   ]$default $2" | tee -a /tmp/biobrain-init.log
            ;;
        scss)
             echo -e "$green[$( date )] [$(basename $0)] [SUCCESS]$default $2" | tee -a /tmp/biobrain-init.log
            ;;
        warn)
            echo -e "$yellow[$( date )] [$(basename $0)] [WARNING]$default $2" | tee -a /tmp/biobrain-init.log
            ;;
        dbg)
            if [[ "$debugMode" == "debug" || "$debugMode" == "-d" ]]; then
                echo -e "$yellow[$( date )] [$(basename $0)] [DEBUG  ]$default $2" | tee -a /tmp/biobrain-init.log
            fi
            ;;
    esac
}

function title() {
  local title="$1"
  local letterCount
  local prefix
  local suffix
  letterCount="$(echo $1 | wc -c)"
  prefix=$(echo \(40-2-$letterCount\)/2 | bc)


  if (( ( 40 - $letterCount ) % 2 == 0)); then
    suffix=$prefix
  else
    suffix=$(echo $prefix+1 | bc)
  fi
  echo "======================================="
  printf "=%$(( $prefix ))s$title%$(( $suffix ))s=\\n"
  echo "======================================="
}

function totalTraffic() {
  local ipAddr="$1"
  local location="$2"
  # Total Tx to ipAddr in Bytes
  tx=$(tshark -r $pcap -Y "(ip.dst==$ipAddr and ip.src==$hostIP)" -T fields -e frame.len | awk '{sum += $1} END {print sum}') || log err "Failed to sum Transmitted Data"
  # Total Rx from ipAddr in Bytes
  rx=$(tshark -r $pcap -Y "(ip.src==$ipAddr and ip.dst==$hostIP)" -T fields -e frame.len | awk '{sum += $1} END {print sum}') || log err "Failed to sum Received Data"
  #Total to and from ipAddr in Bytes
  total=$(tshark -r $pcap -Y "(ip.src==$ipAddr and ip.dst==$hostIP) or (ip.dst==$ipAddr and ip.src==$hostIP)" -T fields -e frame.len | awk '{sum += $1} END {print sum}') || log err "Failed to sum Data"

  title "$location: $ipAddr"
  echo -e "Received:\t$blue$rx bytes$default"
  echo -e "Transmitted:\t$blue$tx bytes$default"
  echo -e "Total:\t\t$blue$total bytes$default\n"

  if [[ ! -z "$total" ]]; then
    grandTotal=$(echo $grandTotal + $total | bc)
  fi
}

function getDNSQueries() { # All DNS Queries not of localhost
  title "DNS Queries"
  tshark -r "$pcap" -T fields  -e dns.qry.name  -Y "dns.flags.response eq 0 and ip.src==$hostIP" | sort | uniq -c | sort -rn | egrep -v "local|(\d{4}-){3}\d{4}" || log err "Unable to find DNS queries"
  echo
}

function getUDP() { # Total Tx and Rx UDP traffic to $digesterProxy in Bytes
  local total
  title "UDP traffic to $digesterProxy in Bytes"
  total=$(tshark -r "$pcap" -Y 'udp and (ip.src==54.85.142.150 or ip.dst==54.85.142.150)' -T fields -e frame.len | awk '{sum += $1} END {print sum}') || log err "Unable to retrieve UDP total"
  echo -e "$digesterProxy:\t$blue$total bytes$default\n"

  if [[ ! -z "$total" ]]; then
    grandTotal=$(echo $grandTotal + $total | bc)
  fi
}

function getBadTCPTraffic() {
  local keepAlive
  local duplicates
  local total

  title 'Bad TCP Traffic'
  total=$(tshark -r $pcap -Y 'tcp.analysis.flags and !tcp.analysis.window_update' -T fields -e frame.time -e frame.len | awk '{sum+=$NF}END{print sum}') || log err "Unable to sum KeepAlive packets"
  keepAlive=$(tshark -r $pcap -Y 'tcp.analysis.flags and !tcp.analysis.window_update and tcp.analysis.duplicate_ack_num > 1' -T fields -e frame.time -e frame.len | awk '{sum+=$NF}END{print sum}') || log err "Unable to sum KeepAlive packets"
  duplicates=$(echo $total - $keepAlive | bc) || log err "Unable to sum KeepAlive packets"

  echo -e "Keep Alive:\t$blue$keepAlive bytes$default"
  echo -e "Duplicates:\t$blue$duplicates bytes$default"
  echo -e "Total:\t\t$blue$total bytes$default"
  echo
}

function getTrafficByIP() {
  local ip
  ip=$(echo $hostIP | awk -F '.' '{print $1"."$2"."$3"."0'})
  title "Traffic by IP"

  echo -e "$yellow""Transmitted:$default"
  tshark -r $pcap -T fields -e ip.dst -Y '!(ip.dst=='"$ip"'/24) and ip.src=='"$hostIP" | grep -vE "^$" | sort -u | while read i; do
    echo -e "$i\t$blue$(tshark -r $pcap -T fields -e frame.len -Y ip.dst==$i | awk '{sum+=$1}END{print sum, bytes}')$default"
  done
  echo

  echo -e "$yellow""Received:$default"
  tshark -r $pcap -T fields -e ip.src -Y '!(ip.src=='"$ip"'/24) and ip.dst=='"$hostIP" | grep -vE "^$" | sort -u | while read i; do
    echo -e "$i\t$blue$(tshark -r $pcap -T fields -e frame.len -Y ip.src==$i | awk '{sum+=$1}END{print sum, bytes}')$default"
  done
  echo
}

function deepDive() {
  echo "==================================================================================="
  echo "=                                   IP Deep Dive                                  ="
  echo "==================================================================================="
  echo "|    ip.src    |    ip.dst    | len | Protocol |            frame.time            |"
  echo "==================================================================================="

  tshark -r $pcap -T fields -e ip.src -e ip.dst -e frame.len -e _ws.col.Protocol -e frame.time -Y "(ip.dst==$hostIP and ip.src==$advIP) or ( ip.dst==$advIP and ip.src==$hostIP)"
}

###########################################
#             Debugging info              #
###########################################
log dbg "Debugging:\tENABLED"
log dbg "pcap\t\t$pcap"
log dbg "hostIP\t\t$hostIP"
log dbg "debugMode\t\t$debugMode"
log dbg "proxy\t\t$proxy"
log dbg "digesterProxy\t$digesterProxy"
log dbg "saltMaster\t$saltMaster"
###########################################

if [[ "$pcap" == "-h" ]]; then
  echo -e "Ussage:\n\t$(basename $0) pcapFile briobrainIP [ deepDiveIP [ -d || debug"
  exit 1
elif [[ -z "$pcap" ||  -z "$hostIP" ]]; then
  log err "Invalid arguments"
  echo -e "Ussage:\n\t$(basename $0) pcapFile briobrainIP [ deepDiveIP [ -d || debug"
  exit 1
fi


if [[ ! -z "$advIP" ]]; then
  log info "Starting Deep Dive on $advIP"
  deepDive
else
  log info "Starting bandwidth audit"
  totalTraffic "$proxy" "Proxy"
  totalTraffic "$saltMaster" "Salt Master"
  totalTraffic "$digesterProxy" "Digester Proxy"
  getUDP
  getBadTCPTraffic
  getDNSQueries
  getTrafficByIP $hostIP

  title "Grand Total"
  echo -e "Grand total:\t$yellow$grandTotal bytes$default"
fi

exit
