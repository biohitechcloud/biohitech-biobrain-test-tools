#!/usr/bin/env bash

# WARNING: This is a legacy script and has been moved to the biobrain-init project"

# Collection of functions to init'd biobrains

# Global Variables
networkWireless="false"
networkPLC="false"
debugMode="$1"
doReboot="false"
readonly thisScript=$(basename $0 | sed 's/\.sh//g')
readonly default="\033[39m"
readonly black="\033[30m"
readonly red="\033[31m"
readonly green="\033[32m"
readonly yellow="\033[33m"
readonly blue="\033[34m"
readonly argsLength=$#

function log {
    case "$1" in
        err)
            echo -e "$red[$( date )] [$(basename $0)] [ERROR  ]$default $2" | tee -a /opt/bht/logs/post-init-test.log
            ;;
        info)
            echo -e "$blue[$( date )] [$(basename $0)] [INFO   ]$default $2" | tee -a /opt/bht/logs/post-init-test.log
            ;;
        scss)
            echo -e "$green[$( date )] [$(basename $0)] [SUCCESS]$default $2" | tee -a /opt/bht/logs/post-init-test.log
            ;;
        dbg)
            if [[ "$debugMode" == "debug" || "$debugMode" == "-d" ]]; then
                echo -e "$yellow[$( date )] [$(basename $0)] [DEBUG  ]$default $2" | tee -a /opt/bht/logs/post-init-test.log
            fi
            ;;
    esac
}

function rootCheck {
    # Check to see if the script was run as Root
    if [[ "$EUID" -ne 0 ]]; then
        log err  "Sorry, you need to run this as root"
        exit 1
    fi
}

function getNetworkAbility {
    wifiModule=$(cat /opt/bht/etc/local.properties  | grep -ioE "^modules=.*" | grep -io "wifi")
    if [[ -d /opt/bht/etc/networks && "$wifiModule" == "wifi" ]]; then
        networkWireless="true"
        log info "Is configured for wireless"
    elif [[ ! -d /opt/bht/etc/networks && "$wifiModule" != "wifi" ]]; then
        log info "Is not configured for wireless"
    elif [[ ! -d /opt/bht/etc/networks ]]; then
        log err "/opt/bht/etc/networks does not exist"
    elif [[ -z "$wifiModule" ]]; then
        log err "wifi module is not in local.properties"
    else
        log err 'Error occurred while trying to determine wireless status'
    fi

    hasPlugable=$(cat /etc/network/interfaces | grep -o 25.25.25.200)
    requiresPlugable=$(cat /opt/bht/etc/local.properties  | grep -ioE "^modules=.*" | grep -ioE "allen-bradley|xgb")
    if [[ ! -z "$hasPlugable" && ! -z $requiresPlugable ]]; then
        networkPLC="t rue"
        log info "Is configured to use plugables dongle"
    elif [[ -z "$hasPlugable" ]]; then
        log err "/etc/network/interfaces is not configured for xgb"
    elif [[ -z "$requiresPlugable" ]]; then
        log err "Not listed as xgb/ab"
    else
        log err "Unable to determine if network should have a plugable dongle"
    fi
}

function plcInterfaceTest {
 # Testing for PLC Interface
    if [[ "$networkPLC" == "true" ]]; then
        if [[ ! -z "$(ifconfig | grep -io "25.25.25.200")" && "$(ifconfig | grep -io "25.25.25.200")" == "25.25.25.200" ]]; then
            log scss "PLC Interface is up and running"
        else
            log err "PLC Interface did not come up"
            while [[ "$plcIsPluggedIn" != "y" && "$plcIsPluggedIn" != "n" ]]; do
                read -p "Is the pluggable dongle plugged in? [Y/n] " plcIsPluggedIn
                plcIsPluggedIn="$(echo $plcIsPluggedIn | tr '[A-Z]' '[a-z]')"
            done

            if [[ "$plcIsPluggedIn" == "y" ]]; then
                doReboot="true"
            elif [[ "$plcIsPluggedIn" == "n" ]]; then
                read -p "Please plug in dongle and press enter :  "

                count=0
                while [[ -z "$(ifconfig | grep -io "25.25.25.200")" && "$(ifconfig | grep -io "25.25.25.200")" == "25.25.25.200" ]]; do
                    if [[ "$count" -gt "9" ]]; then
                        log err "PLC Interface has yet to come up"
                        break;
                    fi
                    let count=$count+1
                    sleep 1
                done

                if [[ "$count" -lt "10" ]]; then
                        log scss "Plc Interface came up"
                fi
            fi
        fi
    fi
}

function wirelessInterfaceTest {
# Testing for Wireless Interface
    if [[ ! -z "$networkWireless" && "$networkWireless" == "true" ]]; then
        log info "Testing wireless interface is configured"
        if [[ ! -z "$(cat /opt/bht/etc/networks/wireless_interfaces | grep -io "bhtwlan0")" ]]; then
            log scss "/opt/bht/etc/networks/wireless_interfaces exists and is configured properly"
        else
            log err "/opt/bht/etc/networks/wireless_interfaces does not exist or is not appropriately configured"
        fi


        log info "Testing wireless interface is up"
        if [[ -z "$(ifconfig | grep -io bhtwlan0)" ]]; then
            doReboot="true"
            log err "Wireless interface is not up"
        fi
    fi
}

function primaryInterfaceTest {
    # Testing primary connection/interface
    if [[ ! -z "$( ifconfig | grep -ioP "inet.*?(\d{1,3}\.){3}\d{1,3}" | grep -ivP "127.0.0.1|25.25.25.200" | sed 's/inet addr://g')" ]]; then
        log scss "Primary interface is up"

        log info "Testing for internet. Pinging google"
        pingTest=$(ping -c 10 google.com | grep -io "Destination host unreachable" | sort | uniq)
        if [[ -z "$pingTest" ]]; then
            log scss "Ping of google successful"
        else
            log err "Could not ping to google"
            return 1
        fi
    fi
}

function testNetworkInterfaces {
    plcInterfaceTest
    wirelessInterfaceTest
    primaryInterfaceTest
}

function getBiobrainPid {
    log info "Looking for BioBrain PID"
    startTime=$(date +%s)
    log dbg "Start Time\t$startTime"
    while [[ -z $pid ]]; do
        if [[ "$(echo "$(date +%s) - $startTime" | bc)" -gt "60" ]]; then
            log err "Could not find BioBrain PID | BioBrain not running..."
            return 1;
        fi
        echo -ne "Time Elapsed $(echo "$(date +%s) - $startTime" | bc) seconds\\r"
        pid=$(ps auxwww | grep java | grep BioBrain | grep -v grep | awk '{print $2}')
        log dbg "Current PID\t$pid"
    done
    log scss "Found BioBrain pid $blue$pid$default"
}

function optBhtPermissionFix {
    count=0
    log info
    while [[ ! -z "$(find /opt/bht/ -exec ls -l {} \; | grep -i "root" )" ]]; do
        if [[ "$count" -gt "4" ]]; then
            log err "Unable to$yellow chown$default /opt/bht/"
            return 1
        fi
        chown -R bht:bht /opt/bht/
        let count=$count+1
    done
    log scss "All files in $yellow/opt/bht/$default now owned by bht"
    log info "Syncing file system"
    sync
}

function interfaceScriptTest {
    prim=$(bash /opt/bht/biobrain/bin/interfaces.sh -p)
    if [[ -z $prim ]]; then
        log err "unable to find primary interface"
    else
        log info "primary interface: $prim"
    fi

    if [[ ! -z $(cat /opt/bht/etc/local.properties  | grep -i modules | egrep -io "allen-bradley|xgb") ]]; then 
        sec=$(bash /opt/bht/biobrain/bin/interfaces.sh -s)
        if [[ -z $sec ]]; then
            log err "unable to find secondary interface"
        else
            log info "secondary interface: $sec"
        fi
    fi    
}

#######################################
#                MAIN                 #
#######################################

log dbg "debuging enabled"

rootCheck

log dbg $networkWireless
log dbg $networkPLC
log dbg $debugMode
log dbg $thisScript
log dbg "$black black"
log dbg "$red red"
log dbg "$green green"
log dbg "$yellow yellow"
log dbg "$blue blue"
log dbg "$default default"
log dbg "$argsLength argsLength"


if [[ "$argsLength" -gt "1" || ( ! -z "$debugMode" && "$debugMode" != "debug" && "$debugMode" != "-d" ) ]]; then
    log err "Invalid arguments"
    echo -e "Ussage:\n\tNORMAL:\n\t\t$(basename $0)\n\tDEBUG:\n\t\t$(basename $0) -d\n\tOR\n\t\t$(basename $0) debug"
    exit 1
fi

interfaceScriptTest
getNetworkAbility
testNetworkInterfaces
getBiobrainPid
if [[ "$?" == "1" ]]; then
    optBhtPermissionFix
    if [[ "$?" == "0" ]]; then
        getBiobrainPid
    fi
fi

log dbg "$doReboot"

if [[ "$doReboot" == "true" ]]; then
    for i in {5..0}; do
        echo -ne "Rebooting in $blue$i$default seconds in an attempt fix issues.\\r"
        sleep 1
    done
    echo -ne "\\r"
    reboot
fi
