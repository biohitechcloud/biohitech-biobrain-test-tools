#!/bin/bash
dongleInterface=$(ifconfig | grep -i '8c:ae:4c' | awk '{print $1}')

# This script runs at start up, and will log the time the script is kick off
# followed by the time it detected a static ip for the plugable dongle. The script
# requires something to kick it off, either an upstart config (/etc/init/*.conf)
# or a systemd service (/etc/systemd/system/*.service)
#
# Upstart info: http://upstart.ubuntu.com/getting-started.html
# Systemd info: https://access.redhat.com/documentation/en-US/Red_Hat_Enterprise_Linux/7/html/System_Administrators_Guide/sect-Managing_Services_with_systemd-Unit_Files.html


# Makes sure that it has a place to log to
if [[ -d "/opt/bht/logs/" ]]; then
  touch /opt/bht/logs/dongle-connect-time.log
else
  mkdir -p /opt/bht/logs/
  touch /opt/bht/logs/dongle-connect-time.log
fi

# main logic
echo >> /opt/bht/logs/dongle-connect-time.log
start=$(date +%s)
echo -e "start:\t$start" >> /opt/bht/logs/dongle-connect-time.log

while [[ -z "$(ifconfig $dongleInterface | grep 25.25.25.200)" ]]; do
  sleep .5
done

stop=$(date +%s)
echo -e "stop:\t$stop"  >> /opt/bht/logs/dongle-connect-time.log

echo -e "Seconds elapsed:\t$(echo $stop - $start | bc )" >> /opt/bht/logs/dongle-connect-time.log

sudo /opt/bht/biobrain/bin/run.sh /opt/bht/biobrain/bin/restart-system.sh
